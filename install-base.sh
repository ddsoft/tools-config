#!/usr/bin/sh

if [ ! -f ~/.vimrc ]; then
    touch ~/.vimrc;
fi

if ! grep -q "vim-basic" ~/.vimrc; then
   echo "adding vim configurations";
   echo "source ~/.my-config/vim-basic" >> ~/.vimrc;
fi

if [ ! -f ~/.bashrc ]; then
    touch ~/.bashrc;
fi

if ! grep -q "bash-basic.sh" ~/.bashrc; then
   echo "adding bash configurations";
   echo "source ~/.my-config/bash-basic.sh" >> ~/.bashrc;
fi

if [ ! -f ~/.gitconfig ]; then
    touch ~/.gitconfig;
fi

if ! grep -q ".my-config" ~/.gitconfig; then
   echo "adding git configurations";
   echo "[include]" >> ~/.gitconfig;
   echo "  path = ~/.my-config/git.conf/gitconfig" >> ~/.gitconfig
fi

